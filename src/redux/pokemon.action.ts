import { ACTION_TYPES } from "./../types/action.types";
export const toggleModal = (data: any) => {
  return {
    type: ACTION_TYPES.MODAL_TOGGLE,
    payload: data,
  };
};
