import { ACTION_TYPES } from "./../types/action.types";
const INITIAL_STATE = {
  error: null,
  loading: false,
  data: [],
};

export const cardReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case ACTION_TYPES.CARD_FETCH:
      return { ...state, loading: true };
    case ACTION_TYPES.CARD_SUCCESS:
      return {
        ...state,
        data: [...state?.data, ...action?.payload],
        loading: false,
      };
    case ACTION_TYPES.CARD_FAILED:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
};
