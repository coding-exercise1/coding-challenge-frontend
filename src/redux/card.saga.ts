import { ACTION_TYPES } from "../types/action.types";
import { call, put, select, take, takeLatest } from "redux-saga/effects";
import {
  fetchAllPokemonData,
} from "../service/pokeApi";

export function* getCard(): Generator<any, void, any> {
  yield put({ type: ACTION_TYPES.CARD_FETCH });
  try {
    yield take(ACTION_TYPES.SUCCESS);
    const {pokemonReducer} = yield select()
    const cardData = yield call(fetchAllPokemonData, pokemonReducer?.results);
    yield put({ type: ACTION_TYPES.CARD_SUCCESS, payload: cardData });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CARD_FAILED, payload: error });
  }
}

export function* watchNext(): Generator<any, void, any> {
  yield take(ACTION_TYPES.NEXT);
  yield take(ACTION_TYPES.SUCCESS);
  const { pokemonReducer } = yield select();
  yield put({ type: ACTION_TYPES.CARD_FETCH });
  try {
    const pokemonData = yield call(fetchAllPokemonData, pokemonReducer?.results);
    yield put({ type: ACTION_TYPES.CARD_SUCCESS, payload: pokemonData });
  } catch (error) {
    yield put({ type: ACTION_TYPES.CARD_FAILED, payload: error });
  }
}

export function* updateCardSaga() {
  yield takeLatest(ACTION_TYPES.CARD_SUCCESS, watchNext);
}
