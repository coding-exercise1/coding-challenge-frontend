import rootReducer from "./rootReducer";
import createSagaMiddleware from "redux-saga";
import { configureStore } from "@reduxjs/toolkit";
import { logger } from "redux-logger";
import rootSaga from "./rootSaga";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: rootReducer,
  middleware: [logger, sagaMiddleware],
});

sagaMiddleware.run(rootSaga);

export default store;
