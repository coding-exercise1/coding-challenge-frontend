import { all } from "redux-saga/effects";
import { getPokemon, updatePokemonSaga, watchScrollLast  } from "./pokemon.sagas";
import { getCard , watchNext , updateCardSaga } from "./card.saga";

export default function* rootSaga(): Generator {
  yield all([watchScrollLast(), getPokemon(), updatePokemonSaga() , getCard() , watchNext() , updateCardSaga()]);
}
