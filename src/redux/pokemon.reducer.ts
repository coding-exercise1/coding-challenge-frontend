import { ACTION_TYPES } from "../types/action.types";

const INITIAL_STATE = {
  count: 0,
  next: null,
  previous: null,
  results: [],
  error: null,
  loading: false,
  modalOpen: false,
  modalData: {},
  modalLoading: false,
  modalError: null,
};

export const pokemonReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case ACTION_TYPES.FETCH:
      return { ...state, loading: true };
    case ACTION_TYPES.SUCCESS:
      return {
        ...state,
        ...action.payload,
        results: action.payload?.results,
        loading: false,
      };
    case ACTION_TYPES.NEXT:
      return {
        ...state,
        loading: true,
      };
    case ACTION_TYPES.FAILED:
      return { ...state, loading: false, error: action.payload };

      
    case ACTION_TYPES.MODAL_TOGGLE:
      return {
        ...state,
        modalOpen: action.payload.isOpen,
        modalData: action.payload.data,
      };

    default:
      return state;
  }
};
