import { cardReducer } from "./card.reducer";
import { combineReducers } from "redux";
import { pokemonReducer } from "./pokemon.reducer";

const rootReducer = combineReducers({
  pokemonReducer,
  cardReducer,
});

export default rootReducer;
