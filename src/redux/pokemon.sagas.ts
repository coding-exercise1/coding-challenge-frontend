import { ACTION_TYPES } from "../types/action.types";
import { call, put, select, take, takeLatest } from "redux-saga/effects";
import {
  fetchAllPokemonData,
  fetchPokemon,
  fetchPokemonNext,
} from "../service/pokeApi";

export function* getPokemon(): Generator<any, void, any> {
  yield put({ type: ACTION_TYPES.FETCH });
  try {
    const pokemon = yield call(fetchPokemon);
    yield put({ type: ACTION_TYPES.SUCCESS, payload: pokemon });
  } catch (error) {
    yield put({ type: ACTION_TYPES.FAILED, payload: error });
  }
}

export function* watchScrollLast(): Generator<any, void, any> {
  yield take(ACTION_TYPES.NEXT);
  const { pokemonReducer } = yield select();
  yield put({ type: ACTION_TYPES.FETCH });
  try {
    const pokemon = yield call(fetchPokemonNext, pokemonReducer?.next);
    yield put({ type: ACTION_TYPES.SUCCESS, payload: pokemon });
  } catch (error) {
    yield put({ type: ACTION_TYPES.FAILED, payload: error });
  }
}

export function* updatePokemonSaga() {
  yield takeLatest(ACTION_TYPES.SUCCESS, watchScrollLast);
}
