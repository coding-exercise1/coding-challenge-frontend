import ReactDOM from "react-dom";
import { useSelector } from "react-redux";
import { PokeData, Stats } from "../types/pokemon.type";
import Chip from "./Chip";
import Ability from "./Ability";

type Props = {
  shown?: boolean;
  close?: () => void;
};

export default function Modal({ shown, close }: Props): JSX.Element {
  document.body.style.overflowY = shown ? "hidden" : "scroll";
  const { modalData } = useSelector((state: any) => state?.pokemonReducer);

  return ReactDOM.createPortal(
    <div className={`${shown ? "" : "pointer-events-none"} fixed top-0`}>
      <div
        className={
          !shown
            ? "hidden"
            : "bg-slate-300 bg-opacity-20 h-screen w-screen backdrop-blur-sm"
        }
        onClick={close}
      ></div>
      <div
        className={`${
          shown ? "opacity-1 -translate-y-1/2" : "opacity-0 -translate-y-3/4"
        } min-h-fit md:w-1/2 w-full bg-white fixed top-1/2 left-1/2 -translate-x-1/2  shadow-lg rounded-md transition-all duration-300 ease-out`}
      >
        <div className=" h-full relative">
          <div className=" absolute -top-14 left-1/2 -translate-x-1/2 text-center">
            <img
              className="drop-shadow-[0_35px_35px_rgba(0,0,0,0.25)] h-24"
              src={modalData?.sprites?.other?.dream_world?.front_default}
              alt={modalData?.name}
            />
            <h1 className="mt-4 font-semibold text-lg capitalize text-gray-500">
              {modalData?.name}
            </h1>
          </div>
          <div className=" h-full p-10">
            <div className="absolute top-2 right-3">
              <button
                onClick={close}
                className=" font-semibold rounded-md px-2 text-sm shadow-md bg-gray-100 hover:bg-gray-200"
              >
                X
              </button>
            </div>

            <div className="h-full pt-12">
              <div className="flex justify-around ">
                <div className="flex flex-col gap-4">
                  <h2 className="font-semibold text-xl text-gray-500">
                    Abilities
                  </h2>
                  <ul className="text-gray-400">
                    {modalData?.abilities?.map((item: any, i: number) => (
                      <Ability key={i} ability={item?.ability} />
                    ))}
                  </ul>
                </div>
                <div className="flex flex-col gap-4">
                  <h2 className="font-semibold text-xl text-gray-500">
                    Types
                  </h2>
                  <div className="flex gap-4">
                    {modalData?.types?.map((item: PokeData, i: number) => (
                      <Chip key={i} type={item as any} />
                    ))}
                  </div>
                </div>
              </div>
                {modalData?.stats?.map((item: Stats, i: number) => (
                  <div key={i}>
                <div className="mb-1 text-gray-500 font-medium capitalize">{item?.stat?.name}</div>
                <div className="flex w-full h-4 bg-gray-200 rounded-full overflow-hidden ">
                  <div className="flex  justify-center overflow-hidden bg-gradient-to-r from-cyan-500 to-blue-500 text-xs text-white text-center" style={{width: `${item?.base_stat}%`}}>{item?.base_stat}%</div>
                </div>

                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>,
    document.getElementById("portal")!
  );
}
