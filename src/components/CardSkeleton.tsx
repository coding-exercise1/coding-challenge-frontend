import ContentLoader from "react-content-loader";

const CardSkeleton = (props: any): JSX.Element => (
  <div className="h-72 w-64 shadow-xl relative overflow-hidden m-10 flex justify-center items-end rounded-lg bg-white">
    <ContentLoader
      speed={2}
      width={256}
      height={288}
      viewBox="0 0 256 288"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <rect x="93" y="85" rx="3" ry="3" width="88" height="6" />
      <rect x="192" y="9" rx="3" ry="3" width="52" height="11" />
      <circle cx="136" cy="44" r="29" />
      <rect x="20" y="138" rx="10" ry="10" width="68" height="10" />
      <rect x="232" y="158" rx="0" ry="0" width="1" height="0" />
      <rect x="102" y="102" rx="10" ry="10" width="68" height="13" />
      <rect x="101" y="139" rx="10" ry="10" width="68" height="10" />
      <rect x="181" y="139" rx="10" ry="10" width="68" height="10" />
      <rect x="19" y="158" rx="10" ry="10" width="68" height="10" />
      <rect x="101" y="159" rx="10" ry="10" width="68" height="10" />
      <rect x="182" y="159" rx="10" ry="10" width="68" height="10" />
    </ContentLoader>
  </div>
);

export default CardSkeleton;
