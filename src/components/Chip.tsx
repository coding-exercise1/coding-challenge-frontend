import React from "react";
import { Types } from "../types/pokemon.type";

type Props = {
  type: Types;
};

export default function Chip({ type }: Props) {
  return (
    <div
      key={type?.slot}
      className={`text-white text-sm font-semibold ${type.type.name} rounded-full px-2 py-1 w-fit shadow-md`}
    >
      <span className="capitalize">{type?.type?.name}</span>
    </div>
  );
}
