import { useDispatch } from "react-redux";
import { toggleModal } from "../redux/pokemon.action";
import { PokeData } from "../types/pokemon.type";
import Chip from "./Chip";

interface CardProps {
  data: PokeData;
  cardRef?: any;
}



export default function Card({ data, cardRef }: CardProps): JSX.Element {
const dispatch = useDispatch();

  const showModal = () => {
    dispatch(toggleModal({ isOpen: true, data }));
  };

  return (
    <div
      onClick={showModal}
      role="card"
      ref={cardRef}
      className="h-72 w-64 shadow-xl relative overflow-hidden m-10 flex justify-center items-end rounded-lg bg-white transition-all duration-200 cursor-pointer hover:-translate-y-1 hover:shadow-2xl active:translate-y-1 active:shadow-md"
    >
      <div
        className={`${data?.types[0].type.name} absolute  -top-3/4 rounded-full w-full h-full scale-150 -z-1`}
      />
      <span className="font-semibold text-gray-600 text-sm absolute top-4 right-4 bg-slate-100 rounded-full px-2">
        {data?.stats[0]?.stat?.name.toUpperCase()} {data?.stats[0]?.base_stat}
      </span>

      <div className="flex flex-col items-center mx-10 my-5 relative bottom-0 w-60 gap-2">
        <img
          src={data?.sprites?.other?.dream_world?.front_default}
          className="h-24 aspect-square"
        />
        <h1 className="font-semibold capitalize text-black tracking-wider">
          {data?.name}
        </h1>
        <div className="flex gap-4 justify-center">
          {data
            ? data?.types?.map((item, i) => <Chip key={i} type={item} />)
            : null}
        </div>

        <div className="font-semibold flex w-full justify-between">
          {data?.stats
            ?.filter((_, index) => index >= 1 && index <= 2)
            .map((item) => (
              <div key={item?.stat?.name} className="text-center w-14">
                <p className="text-black">{item?.base_stat}</p>
                <h3 className="text-gray-600 font-semibold text-sm capitalize">
                  {item?.stat?.name}
                </h3>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}
