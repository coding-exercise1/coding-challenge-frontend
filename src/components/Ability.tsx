import { useEffect, useState } from "react";
import { Ability as AbilityProp, PokemonEffect } from "../types/pokemon.type";
import ContentLoader from "react-content-loader";

type Props = {
  ability: AbilityProp;
};

export default function Ability({ ability }: Props) {
  const [hover, setIsHover] = useState<boolean>(false);
  const [details, setDetails] = useState<PokemonEffect>({} as PokemonEffect);
  const [firstHoverDetails, setFirstHoverDetails] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    if (!firstHoverDetails) return;
    getAbilityDetails();
    return () => {};
  }, [firstHoverDetails]);

  const detailsHover = () => {
    setIsHover(!hover);
    setFirstHoverDetails(true);
  };

  const getAbilityDetails = async () => {
    try {
      const req = await fetch(ability?.url);
      const data = await req.json();
      setDetails(data);
    } catch (error) {
      setError(error as Error);
    }
  };

  return (
    <li className=" flex gap-4 justify-between items-center my-1">
      <p className="text-lg xs:text-sm font-semibold capitalize">{ability?.name}</p>
      <div className="relative border-4 rounded-full font-semibold ">
        <span
          className="w-4 h-4 items-center flex justify-center cursor-help"
          onMouseEnter={detailsHover}
          onMouseLeave={detailsHover}
        >
          ?
        </span>
        <div
          className={`${
            hover ? "opacity-100" : "opacity-0"
          } font-thin text-base transition-all duration-200 w-60 absolute bg-white p-2 rounded-md shadow-lg top-full left-full z-10`}
        >
          {error ? (
            <p>Ooops something goes wrong!</p>
          ) : details?.effect_entries ? (
            details?.effect_entries
              ?.filter((item) => item?.language?.name === "en")
              .map((item, i) => <p key={i}>{item?.effect}</p>)
          ) : (
            <ContentLoader
              speed={2}
              width={225}
              height={64}
              viewBox="0 0 235 64"
              backgroundColor="#f3f3f3"
              foregroundColor="#ecebeb"
            >
              <rect x="2" y="5" rx="3" ry="3" width="230" height="16" />
              <rect x="2" y="25" rx="3" ry="3" width="200" height="16" />
              <rect x="2" y="45" rx="3" ry="3" width="100" height="16" />
            </ContentLoader>
          )}
        </div>
      </div>
    </li>
  );
}
