import { useCallback } from "react";
import { ACTION_TYPES } from "../types/action.types";
import { useDispatch } from "react-redux";
import { useRef } from "react";

type Props = {
  loading?: boolean;
  next?: string;
};

export default function useIntersectionObserver({ loading, next }: Props) {
  const observer = useRef<any>();
  const dispatch = useDispatch();
  const ref = useCallback((node: any) => {
      if (loading) return;
      // Check if there is an observer if TRUE Clear it
      if (observer.current) observer.current.disconnect();
      // create new IntersectionObserver
      observer.current = new IntersectionObserver(
        (entries) => {
          if (entries[0].isIntersecting && next) {
            dispatch({ type: ACTION_TYPES.NEXT });
          }
        },
        {
          threshold: 1,
        }
      );
      // Add observer to the Element
      if (node) observer.current.observe(node);
    },
    [loading]
  );
  return ref;
}
