import {  useRef } from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import Card from '../../components/Card';
import '@testing-library/jest-dom'
import { PokeData } from '../../types/pokemon.type';
import 'isomorphic-fetch';
import { useDispatch } from 'react-redux';
import { toggleModal } from '../../redux/pokemon.action';


const pokemonData : PokeData = {
  name: 'Pikachu',
  stats: [
    { base_stat: 35,  stat: { name: 'hp' } },
  ],
  sprites: {
    other: {
      dream_world: {
        front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg'
      }
    }
  },
  abilities: [
    { ability: { name: 'static', url: 'https://pokeapi.co/api/v2/ability/9/' } },
  ],
  types: [
    { slot:1 ,  type: { name: 'electric' , url: 'https://pokeapi.co/api/v2/type/13/'}} ,
   
  ]
};


jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
}));

// Should make useDespatch() and modal in component to test the card component
describe('Card components' , ()=> {


  let dispatchMock: jest.Mock;

  beforeEach(() => {
    dispatchMock = jest.fn();
    (useDispatch as jest.Mock).mockReturnValue(dispatchMock);
  });


  it('render the Pokemon name', () => {

    render(
    <Card data={pokemonData} />
   );
    const nameElement = screen.getByText('Pikachu');
    expect(nameElement).toBeInTheDocument();
  });
 
  it('render the Pokemon types', () => {

    render(
    <Card data={pokemonData} />
   );
    const nameElement = screen.getByText('electric');
    toggleModal({ isOpen: false, pokemonData })
    expect(nameElement).toBeInTheDocument();
  });

})



