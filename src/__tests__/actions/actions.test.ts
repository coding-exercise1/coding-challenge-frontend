import { toggleModal } from "../../redux/pokemon.action";
import { ACTION_TYPES } from "../../types/action.types";
import { PokeData } from "../../types/pokemon.type";

const pokemonData : PokeData = {
    name: 'Pikachu',
    stats: [
      { base_stat: 35,  stat: { name: 'hp' } },
    ],
    sprites: {
      other: {
        dream_world: {
          front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/25.svg'
        }
      }
    },
    abilities: [
      { ability: { name: 'static', url: 'https://pokeapi.co/api/v2/ability/9/' } },
    ],
    types: [
      { slot:1 ,  type: { name: 'electric' , url: 'https://pokeapi.co/api/v2/type/13/'}} ,
     
    ]
  };


  describe('toggleModal', () => {
    it('should create an action to toggle Modal with pokemon details ', () => {
      const data = pokemonData;
      const expectedAction = {
        type: ACTION_TYPES.MODAL_TOGGLE,
        payload: data,
      };
      expect(toggleModal(data)).toEqual(expectedAction);
    });
  });

