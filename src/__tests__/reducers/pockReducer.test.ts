import { pokemonReducer } from "../../redux/pokemon.reducer";
import { ACTION_TYPES } from "../../types/action.types";
import 'isomorphic-fetch';



describe('pockemonReducer', () => {
    const INITIAL_STATE = {
        count: 0,
        next: null,
        previous: null,
        results: [],
        error: null,
        loading: false,
        modalOpen: false,
        modalData: {},
        modalLoading: false,
        modalError: null,
      };

    it('should return the initial state', () => {
        expect(pokemonReducer(undefined, {type: "" , payload:""})).toEqual(INITIAL_STATE);
    });

    it('should handle Pokemon FETCH', () => {
        expect(pokemonReducer(INITIAL_STATE, {type: ACTION_TYPES.FETCH , payload: true})).toEqual({
            ...INITIAL_STATE,
            loading: true
        });
    });

    it('should handle Pokemon SUCCESS', async  () => {

        const pokemonData  = await fetch("https://pokeapi.co/api/v2/pokemon/?limit=20&offset=0")

        const data = await pokemonData.json();
        const state =  pokemonReducer(INITIAL_STATE, {type: ACTION_TYPES.SUCCESS , payload: {data}})

        expect(state.loading).toEqual(false);
        expect(state.data.count).toEqual(data.count);
        expect(state.data.next).toEqual(data.next);
        
    });

    it('should handle Pokemon NEXT', () => {
        const state = pokemonReducer(INITIAL_STATE, {type: ACTION_TYPES.NEXT , payload: true})
        expect(state).toEqual({
            ...INITIAL_STATE,
            loading: true
        });
    });

    it('should handle Pokemon FAILED', () => {
        const error = "Error fetch data";
        const state = pokemonReducer(INITIAL_STATE, {type: ACTION_TYPES.FAILED , payload: {error}})
        expect(state.loading).toEqual(false);
        expect(state.error.error).toEqual(error);
    });


});