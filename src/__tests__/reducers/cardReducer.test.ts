import { cardReducer } from "../../redux/card.reducer";
import 'isomorphic-fetch';
import { ACTION_TYPES } from "../../types/action.types";


describe('cardReducer', () => {
    const INITIAL_STATE = {
        error: null,
        loading: false,
        data: [],
      };


    it('should return the initial state', () => {
        expect(cardReducer(undefined, {type: "" , payload:""})).toEqual(INITIAL_STATE);
    });


    it('should handle Card FETCH', () => {
        expect(cardReducer(INITIAL_STATE, {type: ACTION_TYPES.CARD_FETCH , payload: true})).toEqual({
            ...INITIAL_STATE,
            loading: true
        });
    });

    it('should handle Card SUCCESS', async () => {

        const fetchData = await fetch("https://pokeapi.co/api/v2/pokemon/1/")

        const pokemonData = await fetchData.json();

      
        const state =  cardReducer(INITIAL_STATE, {type: ACTION_TYPES.CARD_SUCCESS , payload: [pokemonData] })

        
        
         expect(state.loading).toEqual(false);
        expect(state.data).toEqual([pokemonData]);
    });

    it('should handle Card FAILED', () => {
        const error = "Error fetch data";
        const state = cardReducer(INITIAL_STATE, {type: ACTION_TYPES.CARD_FAILED , payload: {error}})
        expect(state.loading).toEqual(false);
        expect(state.error.error).toEqual(error);
    });
});


  

