import { PokeData } from "../types/pokemon.type";

const API = import.meta.env.VITE_POKE_API;

export const fetchPokemon = async () => {
  const req = await fetch(`${API}pokemon?offset=0&limit=12`);
  const data = await req.json();
  return data;
};

export const fetchAllPokemonData = async (data: any[]) => {
  const req = await Promise.all([
    ...data?.map((item: { name: string; url: string }): Promise<any> => {
      return fetchPokemonByUrl(item?.url);
    }),
  ]);

  const response = await req;
  return response;
};

export const fetchPokemonByUrl = async (url: string) => {
  const req = await fetch(url);
  const data: PokeData[] = await req.json();
  return data;
};

export const fetchPokemonNext = async (url: string) => {
  const req = await fetch(`${url}`);
  const data = await req.json();
  return data;
};
