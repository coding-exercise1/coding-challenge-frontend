export type Types = {
  slot: number;
  type: {
    name: string;
    url: string;
  };
};
export type Stats = { base_stat: number; stat: { name: string } };
export type PokeData = {
  name: string;
  stats: Stats[];
  sprites: { other: { dream_world: { front_default: string } } };
  abilities: [{ ability: { name: string; url: string } }];
  types: Types[];
};

export type Ability = {
  name: string;
  url: string;
};

export type PokemonEffectEntries = {
  language: { name: string };
  effect: string;
  short_effect: string;
};

export type PokemonEffect = {
  effect_entries: PokemonEffectEntries[];
};
