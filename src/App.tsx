import Card from "./components/Card";
import errorGif from "./assets/errorGif.gif";
import loadingGif from "./assets/loading.gif";
import { useDispatch, useSelector } from "react-redux";
import Modal from "./components/Modal";
import useIntersectionObserver from "./hooks/useIntersectionObserver";
import { toggleModal } from "./redux/pokemon.action";

function App(): JSX.Element {
  const { error, loading, next, modalOpen } = useSelector(
    (state: any) => state?.pokemonReducer
  );
  const { data } = useSelector((state: any) => state?.cardReducer);

  const ref = useIntersectionObserver({ loading, next });
  const dispatch = useDispatch();

  const handleCloseModal = () => {
    dispatch(toggleModal({ isOpen: false, data: {} }));
  };

  return (
    <>
      <Modal shown={modalOpen} close={handleCloseModal} />
      <div className="flex flex-wrap justify-center container mx-auto">
        {error ? (
          <div className="h-screen w-screen flex justify-center items-center">
            <img src={errorGif} alt="Error" />
          </div>
        ) : data.length ? (
          data?.map((item: any, index: any) =>
            index === data?.length - 1 ? (
              <Card cardRef={ref} key={index} data={item} />
            ) : (
              <Card key={index} data={item} />
            )
          )
        ) : null}
      </div>
      <div className="flex w-screen justify-center h-24 py-4">
        {loading ? <img src={loadingGif} alt="Loading ..." /> : null}
      </div>
    </>
  );
}

export default App;
