# Coding challenge - Frontend

This application uses Poke API to get a list of Pokemons and display them on a web page.
The webpage displays each Pokemon's image, name, and type in a card format and users can click on any Pokemon card to view its details.

> To use this application, please follow the steps below:

1. Ensure that you have the latest version of Node.js installed on your machine.
2. Run the command `npm install` to install all the necessary dependencies.
3. Once the dependencies are installed, run the command `npm run dev` to start the application.
